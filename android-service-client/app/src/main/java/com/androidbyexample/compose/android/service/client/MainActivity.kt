package com.androidbyexample.compose.android.service.client

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.Button
import androidx.compose.material3.LinearProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.androidbyexample.compose.android.service.client.ui.theme.AndroidserviceclientTheme
import com.androidbyexample.compose.android.services.Person
import com.androidbyexample.compose.android.services.RemoteService
import com.androidbyexample.compose.android.services.RemoteServiceReporter

class MainActivity : ComponentActivity() {
    private var progressState by mutableStateOf(0)
    private var peopleState by mutableStateOf("")

    private val reporter = object: RemoteServiceReporter.Stub() {
        override fun report(people: MutableList<Person>, n: Int) {
            progressState = n
            peopleState = people.joinToString("\n") {
                "${it.name}: ${it.age}"
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            AndroidserviceclientTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colorScheme.background) {
                    Ui(progressState, peopleState) {
                        binder?.reset()
                    }
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()
        // to talk with a service in another application, we need to set the className
        //   in the Intent. It takes two parameters
        //      packageName: The application id of the app that hosts the service
        //      className: The fully-qualified class name of the service to bind
        val intent = Intent().apply {
            setClassName(
                "com.androidbyexample.compose.android.services",
                "com.androidbyexample.compose.android.services.RemoteServiceImpl"
            )
        }
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE)
    }

    override fun onStop() {
        binder?.remove(reporter)
        unbindService(serviceConnection)
        super.onStop()
    }

    private var binder: RemoteService? = null

    private val serviceConnection = object: ServiceConnection {
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            binder = RemoteService.Stub.asInterface(service).apply {
                add(reporter)
            }
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            binder = null
            TODO("Not yet implemented")
        }

    }
}

@Composable
fun Ui(
    progress: Int,
    people: String,
    onReset: () -> Unit
) {
    Column(modifier = Modifier.fillMaxSize()) {
        Button(onClick = onReset) {
            Text(text = stringResource(id = R.string.reset))
        }
        Text(text = people)
        var progressValue = progress.toFloat()/100
        if (progressValue > 1.0f) {
            progressValue = 1.0f
        }
        LinearProgressIndicator(
            progress = progressValue,
            modifier = Modifier.fillMaxWidth()
        )
    }
}
