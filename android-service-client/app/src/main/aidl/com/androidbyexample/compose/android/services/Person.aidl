package com.androidbyexample.compose.android.services;

// AIDL that declares (to the AIDL compiler) that we have a class named
//    com.androidbyexample.compose.android.services.Person that's Parcelable
parcelable Person;